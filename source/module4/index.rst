.. _module4:

*************************************************************************************************
Module 4 | Software Engineering Topics: Testing, Design patterns
*************************************************************************************************

Objective
=========

* Understanding control flows
* Designing test cases and how to use code coverage
* Design patterns

Resources
=======================================


Slides (keynote)

* :download:`Software testing (PDF file) <./software_testing.pdf>`

Exercises: week 1
=======================================

* `Blackbox testing <https://inginious.info.ucl.ac.be/course/LEPL1402/BlackBox>`_
* `Coverage testing <https://inginious.info.ucl.ac.be/course/LEPL1402/Coverage>`_