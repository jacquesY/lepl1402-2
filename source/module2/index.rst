.. _module2:


*************************************************************************************************************************************
Module 2 | Complexity, Recursive Programming, Arborescent data structures, Basic algorithms, Invariants and proof of correctness
*************************************************************************************************************************************

Objective
=========

By the end of this module, students will be able to:

* Use (and explain) spatial and temporal complexity concepts, along with the mathematical tools used to describe them
  (big-Oh/Omega/Theta notations and definitions).
* Analyse complex algorithm and derive their complexity
* Code a binary search
* Code the merge sort algorithm
* Explain the RAM model
* Use recursive programming
* Use arborescent data structures (trees, lists, ...)
* Use invariants to show both correctness and complexity of an algorithm


Resources
=======================================

Slides (keynote)

* `Course 2a - Complexity <https://www.icloud.com/keynote/0bLkKAW-hohVQRrH29TBVJfuA#cours2a-complexity_-_copie>`_

.. raw:: html

	
	<iframe width="560" height="315" src="https://www.youtube.com/embed/zu5HKJ07jzo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


* `Course 2b - ADT (collections) and proof of programs <https://www.icloud.com/keynote/0rn26lkOFEQaPybTU6oGOZTsw#cours2b-invariants-recursion>`_

.. raw:: html

	<iframe width="560" height="315"  style="padding: 0;" allowfullscreen="allowfullscreen" frameborder="0" scrolling="no" src="https://ezcast.uclouvain.be/ezmanager/distribute.php?action=embed&album=LEPL1402-2019-pub&asset=2019_10_10_16h11_30s&type=slide&quality=high&token=ZPSTZJIV&width=1280&height=720&lang=fr"><a href="https://ezcast.uclouvain.be/ezmanager/distribute.php?action=embed&amp;album=LEPL1402-2019-pub&amp;asset=2019_10_10_16h11_30s&amp;type=slide&amp;quality=high&amp;token=ZPSTZJIV">Voir la vidéo</a></iframe>


Exercises: week 1
=======================================

.. note::
   Exercises due for Thursday S4.

1. `Time Complexity: simple MCQ <https://inginious.info.ucl.ac.be/course/LEPL1402/ComplexityMCQ1>`_
2. `Space Complexity MCQ <https://inginious.info.ucl.ac.be/course/LEPL1402/ComplexitySpaceMCQ>`_
3. `Array Search <https://inginious.info.ucl.ac.be/course/LEPL1402/ComplexityArraySearch>`_
4. `Merge Sort <https://inginious.info.ucl.ac.be/course/LEPL1402/MergeSortImplementation>`_
5. `Largest Sum Contiguous Subarray <https://inginious.info.ucl.ac.be/course/LEPL1402/MaximumSumSubarray>`_

Exercises: week 2
=======================================

.. note::
   Exercises due for Thursday S5.

1. `Recursion Fibonacci <https://inginious.info.ucl.ac.be/course/LEPL1402/Fibonacci>`_
2. `Bubble Sort Invariant <https://inginious.info.ucl.ac.be/course/LEPL1402/BubbleSortInvariant>`_
3. `Recursion Hanoi Tower <https://inginious.info.ucl.ac.be/course/LEPL1402/HanoiTower>`_
4. `Longest Valley in an Array <https://inginious.info.ucl.ac.be/course/LEPL1402/valley>`_
5. `Implement a stack with a queue <https://inginious.info.ucl.ac.be/course/LEPL1402/StackWithQueue>`_
6. `Implement a circular linked list <https://inginious.info.ucl.ac.be/course/LEPL1402/CircularLL>`_
7. `Implement a queue with two stacks <https://inginious.info.ucl.ac.be/course/LEPL1402/QueueWithStacks>`_
8. `Implement an array list <https://inginious.info.ucl.ac.be/course/LEPL1402/MyArrayList>`_
