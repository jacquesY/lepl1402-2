.. _module3:


************************************************************************************************************
Module 3 | Arborescent data structures, Object Oriented Programming (inheritance, polymorphism, delegation)
************************************************************************************************************

Objective
=========

By the end of this module, students will be able to:

* Use and implement arborescent and recursive data structures (trees, lists, ...)
* Understand and implement traversal algorithms over trees and lists (prefix, infix, suffix)
* Be able to analyze the time complexity of algorithms over arborescent data-structures
* Understand serialization of arborescent data structures

Resources
=======================================


Slides (keynote)

* `Feedback Inginious <https://www.icloud.com/keynote/0zQ0V3dTZcQPm9QUP54UM9E0A#cours3a-feedback-inginious-gderval>`_
* `Arborescent and recursive ADT <https://www.icloud.com/keynote/0dDho7r2GPnohjW_2NfxvU_lg#cours3a-arborescent-structures>`_
* `Object Oriented Programming Concepts <https://www.icloud.com/keynote/0E9PWUKqyuydFjSsl-2FnedwQ#3b-object-oriented-programming>`_

.. raw:: html

    <iframe width="560" height="315"  style="padding: 0;" allowfullscreen="allowfullscreen" frameborder="0" scrolling="no" src="https://ezcast.uclouvain.be/ezmanager/distribute.php?action=embed&album=LEPL1402-2019-pub&asset=2019_10_17_16h08_27s&type=slide&quality=high&token=JMOTRZTQ&width=1280&height=720&lang=fr"><a href="https://ezcast.uclouvain.be/ezmanager/distribute.php?action=embed&amp;album=LEPL1402-2019-pub&amp;asset=2019_10_17_16h08_27s&amp;type=slide&amp;quality=high&amp;token=JMOTRZTQ">Voir la vidéo</a></iframe>

.. raw:: html

	<iframe width="560" height="315" src="https://www.youtube.com/embed/2dVAg_Rdkd8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>    

Files used in the slides:

* RecursiveList.java :download:`java <RecursiveList.java>`
* RecursiveTree.java :download:`java <RecursiveTree.java>`
* BinaryExpressionTree.java :download:`java <BinaryExpressionTree.java>`


Exercises: week 1
=======================================

.. note::
   Exercises due to Thursday S6.

1. `Equality test between trees <https://inginious.info.ucl.ac.be/course/LEPL1402/TreeSame>`_
2. `Inorder traversals <https://inginious.info.ucl.ac.be/course/LEPL1402/TreeInorder>`_ 
3. `Combining two trees <https://inginious.info.ucl.ac.be/course/LEPL1402/TreeCombineWith>`_ 
4. `MCQ on time complexities <https://inginious.info.ucl.ac.be/course/LEPL1402/complexityMCQ2>`_ 


Exercises: week 2
=======================================

.. note::
   Due to Thursday S8.


1. `Comparator vs Comparable <https://inginious.info.ucl.ac.be/course/LEPL1402/ComparatorvsComparable>`_
2. `Abstract class <https://inginious.info.ucl.ac.be/admin/LEPL1402/edit/task/AbstractClass>`_ 
3. `Comparator Comparable <https://inginious.info.ucl.ac.be/course/LEPL1402/ComparatorvsComparable>`_ 

Design exercise
----------------


Implement this interface with three different classes `Circle, Square, Triangle`.

.. code-block:: java

   public interface Shape {
  		public void draw();
  		public void erase();
   }

Each method should print something to the console like "drawing a circle".
Now implement one class RandomShape that randomly draw a circle, a square or a triangle by flipping a coin.
What are the OOP principles that you are using here ?
Can your design easily be used to also include rectangles in the RandomShape ?



